package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage verifyLeadName(String data)
	{
		String name = driver.findElementById("viewLead_firstName_sp").getText();
		if(name.equals(data))
		{
			System.out.println("Lead created successfully");
		}
		else
		{
			System.out.println("Lead not created");
		}
		return this;
	}

}
