package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC002CreateLeadTest extends Annotations {
	@BeforeTest
	public void setData() {
		excelFileName = "TC001";
	}
	
	@Test(dataProvider = "fetchData")
	public void TC002CreateLead(String userName, String password, String logInName,String companyname,String firstname,String lastname) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.clickCRMSFA()
		.clickLeadsTab()
		.clickCreateLead()
		.EnterCompanyName(companyname)
		.EnterFirstName(firstname)
		.EnterLastName(lastname)
		.ClickCreateLeadButton()
		.verifyLeadName(firstname);
		
	}
	

}
